function [best_particles,best_scores] = particle_filter( objective_fun, range, variance, Nparticles, Ncycles, varargin )
%
% [best_particles,best_scores] = PARTICLE_FILTER( objective_fun, range, variance, Nparticles, Ncycles )
%
% ----------
% INPUTS:
%
% objective_fun
%   Function handle which, given a particle, returns a positive score to be maximised.
%
% range
%   A Ndim x 2 array in which each row specifies the min/max value in each dimension.
%
% variance
%   A Ndim x 1 array with variances in each dimension used during resampling.
%
% Nparticles
%   Some large integer (>30).
%
% Ncycles
%   Number of cycles to run (>20).
%
% ----------
% OPTIONS (key/value pairs):
%
% Nnew
%   Number of new particles to generate at each cycle.
%   DEFAULT: Nparticles/20
%
% BestK
%   Number of best particles to return in output.
%   DEFAULT: 5
%
% BorderControl
%   Actively control that particles never go outside the specified range.
%   DEFAULT: false
%
% ----------
% OUTPUTS:
%
% best_particles
%   The particle(s) that obtained the best score(s) throughout all cycles.
%
% best_scores
%   The record of all best scores at each cycle (matrix 1+Ncycles x BestK, last row most recent).
% 

    % parse options
    opt  = struct();
    arg  = { 'Nnew', ceil(Nparticles/20), 'BestK', 5, 'BorderControl', false }; % defaults
    nopt = numel(arg)/2;
    arg  = [ arg, varargin ];
    narg = numel(arg)/2;
    
    for i = 1:narg
        opt.(arg{ 2*i-1 }) = arg{2*i};
    end
    
    % check inputs
    assert( Nparticles > 30, 'More particles please.' );
    assert( Ncycles > 20, 'More cycles please.' );
    assert( all(variance >= 0), 'Variances should be positive.' );
    assert( all(range(:,2) > range(:,1)), 'Second column should be larger than first column in range.' );
    
    assert( numel(fieldnames(opt)) == nopt, 'Wrong number of options; check that your input keys match option names.' );
    assert( opt.Nnew  < Nparticles, 'Nnew cant be larger than Nparticles.' );
    assert( opt.BestK < Nparticles, 'BestK cant be larger than Nparticles.' );
    assert( opt.BestK > 0, 'BestK should be non-zero.' );
    
    % initialise the particles
    fprintf('Particle filter starting with %d particles and %d cycles:\n',Nparticles,Ncycles);
    fprintf('\t- Initialisation\n');
    
    particles = uniform_sampling( range, Nparticles );
    scores    = evaluate( objective_fun, particles );
    
    % initialise output
    [~,order] = sort(scores,'descend');
    order     = order(1:opt.BestK);
    
    best_scores    = ones(Ncycles+1,1) * scores(order);
    best_particles = particles(:,order);
    
    for c = 1:Ncycles
        
        fprintf('\t- Cycle #%d / %d (started @%s)...\n',c,Ncycles,datestr(now,'dd-mmm-yy HH:MM:SS'));
        
        % resample
        particles = horzcat( ...
            importance_sampling( particles, scores, variance, Nparticles-opt.Nnew ), ...
            uniform_sampling( range, opt.Nnew ) );
        
        if opt.BorderControl
            particles = max( particles, (range(:,1)+eps)*ones(1,Nparticles) );
            particles = min( particles, (range(:,2)-eps)*ones(1,Nparticles) );
        end
        
        % evaluate
        scores = evaluate( objective_fun, particles );
        
        % update best particles
        [~,order] = sort([scores,best_scores(c,:)],'descend');
        order     = order(1:opt.BestK);
        id_keep   = order( order >  Nparticles ) - Nparticles;
        id_new    = order( order <= Nparticles );
        
        best_scores(c+1,:) = [ best_scores(c,id_keep), scores(id_new) ];
        best_particles     = [ best_particles(:,id_keep), particles(:,id_new) ];
        
    end
    
    fprintf('Done!\n');

end

function particles = uniform_sampling( range, n )

    Vmin  = range(:,1);
    Vmax  = range(:,2);
    Ndims = size(range,1);
    
    particles = bsxfun( @times, rand(Ndims,n), Vmax-Vmin );
    particles = bsxfun( @plus, particles, Vmin );

end

function scores = evaluate( objective_fun, particles )

    n = size(particles,2);
    scores = zeros(1,n);
    
    for i = 1:n
        scores(i) = objective_fun(particles(:,i));
    end

end

function particles = importance_sampling( particles, scores, variance, n )

    % cumulative distribution 
    gamma = cumsum(scores);
    gamma = [0 gamma] / gamma(end);
    
    % pick particles
    index = rand(1,n);
    for i = 1:n
        index(i) = find( index(i) > gamma, 1, 'last' );
    end
    
    % add noise
    Ndims     = size(particles,1);
    noise     = bsxfun( @times, randn(Ndims,n), variance );
    particles = bsxfun( @plus, particles(:,index), noise );
    
end
